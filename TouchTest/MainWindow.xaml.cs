﻿using System;
using System.Windows;
using System.Windows.Input;

namespace TouchTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Grid_OnPreviewTouchDown(object sender, TouchEventArgs e)
        {
            Console.WriteLine("Grid Preview Touchdown");
        }

        private void Grid_OnTouchDown(object sender, TouchEventArgs e)
        {
            Console.WriteLine("Grid Touchdown");
        }

        private void FormsHost_OnTouchDown(object sender, TouchEventArgs e)
        {
            Console.WriteLine("FormsHost Touchdown");
        }

        private void FormsHost_OnPreviewTouchDown(object sender, TouchEventArgs e)
        {
            Console.WriteLine("FormsHost Preview Touchdown");
        }

        private void MainWindow_OnTouchDown(object sender, TouchEventArgs e)
        {
            Console.WriteLine("MainWindow Touchdown");
        }

        private void MainWindow_OnPreviewTouchDown(object sender, TouchEventArgs e)
        {
            Console.WriteLine("MainWindow Preview Touchdown");
        }
    }
}
